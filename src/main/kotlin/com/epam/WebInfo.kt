package com.epam

import com.datastax.driver.mapping.annotations.Column
import com.datastax.driver.mapping.annotations.Table
import java.io.Serializable
import java.util.*


/**
 * @author Dmitrii_Kniazev
 * @since 12/30/2016
 */


@Table(keyspace = "flinksink", name = "webinfo")
internal class WebInfo(
        @Column(name = "ip")
        var id: String,
        @Column(name = "posted_time")
        var postedTime: Date,
        @Column(name = "body")
        var body: String) : Serializable {
    companion object {
        private val serialVersionUID = 1038054554690916991L
    }

    override fun toString(): String {
        return "$id:$postedTime:$body"
    }
}


