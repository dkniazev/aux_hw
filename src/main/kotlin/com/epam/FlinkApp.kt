package com.epam


import com.datastax.driver.core.Cluster
import org.apache.flink.api.java.tuple.Tuple
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.cassandra.CassandraSink
import org.apache.flink.streaming.connectors.cassandra.ClusterBuilder
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09
import org.apache.flink.streaming.util.serialization.SimpleStringSchema
import org.apache.log4j.Logger
import java.util.*

/**
 * @author Dmitrii_Kniazev
 * @since 12/30/2016
 */

class FlinkApp {
    companion object {
        private val LOG = Logger.getLogger(FlinkApp::class.java)
        private const val KAFKA_HOST = "localhost:9092"
        private const val KAFKA_GROUPID = "aux"
        private const val KAFKA_TOPIC_NAME = "auxsrc"

        private const val CASSANDRA_HOST = "192.168.99.100"

        @JvmStatic fun main(args: Array<String>): Unit {
            val streamEnv = StreamExecutionEnvironment.getExecutionEnvironment()

            val properties = Properties()
            properties.setProperty("bootstrap.servers", KAFKA_HOST)
            properties.setProperty("group.id", KAFKA_GROUPID)

            val kafkaSrc = streamEnv.addSource(FlinkKafkaConsumer09(KAFKA_TOPIC_NAME, SimpleStringSchema(), properties))
            LOG.info("Kafka input stream created")
            kafkaSrc.print()

            val cassandraStream = kafkaSrc.map { it.split(",") }.filter { it.size == 2 }.map { str2WebInfo(it) }
            cassandraStream.print()
            // The Cassandra sinks support both tuples and POJO’s that use DataStax annotations.
            // Flink automatically detects which type of input is used.
            CassandraSink.addSink<WebInfo, Tuple>(cassandraStream)
                    .setClusterBuilder(object : ClusterBuilder() {
                        override fun buildCluster(builder: Cluster.Builder): Cluster {
                            return builder.addContactPoint(CASSANDRA_HOST).build()
                        }
                    }).build()
            LOG.info("Cassandra output stream created")

            streamEnv.execute("Kafka To Cassandra Stream")
        }

        private fun str2WebInfo(split: List<String>): WebInfo {
            val ip = split[0]
            val body = split[1]
            val timestamp = Date()
            return WebInfo(ip, timestamp, body)
        }
    }
}